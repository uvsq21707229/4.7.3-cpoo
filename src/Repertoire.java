import java.util.ArrayList;

public class Repertoire extends Racine{
	ArrayList<Racine> e;
	public Repertoire (String nom)
	{
		super(nom);
		e = new ArrayList<>();
	}
	public String getnom()
	{
		return this.name;
	}
	public double getsize()
	{
		double s=0;
		for (Racine a : e)
		{
			s=s+a.getsize();	
		}
		
		return s;
	 }
	public boolean ajouter(Racine r)
	{
		
		if((this.verif_repertoire_inside_repertoire(r)==false)||(this.verif_repertoire_inside_Descendant(r)==true))
		{
			return false;
		}
		else 
		{
			e.add(r);
			return true;
		}
		
	}
	// Question 4 
		public boolean verif_repertoire_inside_repertoire(Racine r)
		{
			if(this.equals(r))
			{
				return false;
			}	
			else 
			{
				return true;
			}
			
		}
		
		// Question 5 
		
		public boolean verif_repertoire_inside_Descendant(Racine r)
		{
			boolean verif = false;
			
				for(Racine t : this.e)
					{
					if(t.equals(r))
					{
						verif=true;
						break;
					}
					if(t.getClass()==Repertoire.class && verif==false)
						{
							verif|=((Repertoire)t).verif_repertoire_inside_Descendant(r);
							if(verif)
							{
								break;
							}
						}
					
					}
			return verif;
		}
}

